## Installation du projet

Récupération du projet

1. Clone the project from : https://BlindTravel@bitbucket.org/BlindTravel/tpwebservices.git
2. Pull the dev branch.

---

Installation de la bdd

1. Donwload MongoDB and install it. 
2. Create a Database named : 'Bank' and 'Bank2'.

---

Documentation

1. Ouvrir le fichier situé à la base du projet 'documentation.yaml'.
2. Coller le contenue du fichier sur Swagger Editor.

---

Launch project

1. Download and install NodeJS.
2. Laucnh node.js command prompt.
3. Launch Server with the command : 'nodemon server.js'.
4. Launch the second Server with the command : 'nodemon server2.js'.
